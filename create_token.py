import requests
import argparse

def generateAPIKey(token, tokenname, userid):
    # https://docs.gitlab.com/ee/api/users.html#create-a-personal-access-token
    url = "https://gitlab.com/api/v4/users/{0}/personal_access_tokens".format(userid)
    #token = keyring.get_password("gitlab", "token")
    header = {
        "Private-Token": token
    }
    data = {
        "name": tokenname, 
        "scopes[]": "api"
        }
    res = requests.post(url, headers=header, data=data)  
    res_x = res.json()
    return res_x
    

def generateProjectToken(token, projId,  tokenname, username):
    # https://docs.gitlab.com/ee/api/deploy_tokens.html#create-a-project-deploy-token
    url = "https://gitlab.com/api/v4/projects/{0}/deploy_tokens".format(projId)
    header = {
        "Private-Token": token,
        "Content-Type": "application/json",
    }
    data = '{"name": "' + tokenname + '", "username": "' + username + '", "scopes": ["read_repository"]}'
    res = requests.post(url, headers=header, data=data)
    res_x = res.json()
    return res_x

def generateGroupToken(token, groupId,  tokenname, username):
    # https://docs.gitlab.com/ee/api/deploy_tokens.html#create-a-project-deploy-token
    url = "https://gitlab.com/api/v4/groups/{0}/deploy_tokens".format(groupId)
    header = {
        "Private-Token": token,
        "Content-Type": "application/json",
    }
    data = '{"name": "' + tokenname + '", "username": "' + username + '", "scopes": ["read_repository"]}'
    res = requests.post(url, headers=header, data=data)
    res_x = res.json()
    return res_x


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-accesstoken", help="The GitLab access token to create the new token.",
                    type=str, required=True)
    parser.add_argument("type", help="The type of token requested (AK=Api Key, GT=Group Token, PT=Project Token)",
                    type=str, choices=['AK','GT','PT'])
    parser.add_argument("-tokenname", help="Name of the token you would like to create",
                    type=str, required=True)
    parser.add_argument("-projid", help="GitLab project ID",
                    type=int, default=0)
    parser.add_argument("-groupid", help="GitLab group ID",
                    type=int, default=0)
    parser.add_argument("-username", help="Name of the user you would like to create the token for",
                    type=str, default='NA')
    parser.add_argument("-userid", help="GitLab user ID",
                    type=str, default= 'NA')
    
    args = parser.parse_args()
    
    if args.type == 'AK':
        print("Generating API Key")
        try:
            print(generateAPIKey(args.accesstoken, args.tokenname, args.userid))
        except Exception as e:
            print(e)
    elif args.type == 'GT':
        print("Generating Group Token")
        try:
            print(generateGroupToken(args.accesstoken, args.groupid, args.tokenname, args.username))
        except Exception as e:
            print(e)
    elif args.type == 'PT':
        print("Generating Project Token")
        try:
            print(generateProjectToken(args.accesstoken, args.projid, args.tokenname, args.username))
        except Exception as e:
            print(e)
    else:
        print("Please specify token")
